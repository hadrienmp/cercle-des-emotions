import { defineConfig } from "vite";
import elmPlugin from "vite-plugin-elm";

export default defineConfig(() => {
  return {
    plugins: [elmPlugin()],
    base: process.env.VITE_BASE || "",
  };
});
