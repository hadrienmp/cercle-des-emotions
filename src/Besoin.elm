module Besoin exposing (..)

import AssocList
import Type


relation : List Type.Item
relation =
    [ { libelle = "Affection", id = Type.Id "UvSm" }
    , { libelle = "Aide", id = Type.Id "DMPC" }
    , { libelle = "Amitié", id = Type.Id "TMpj" }
    , { libelle = "Amour", id = Type.Id "ikI9" }
    , { libelle = "Appartenance", id = Type.Id "Bl63" }
    , { libelle = "Authenticité", id = Type.Id "tDtj" }
    , { libelle = "Bienveillance", id = Type.Id "y9MA" }
    , { libelle = "Calme", id = Type.Id "MKyT" }
    , { libelle = "Chaleur", id = Type.Id "ep3L" }
    , { libelle = "Considération", id = Type.Id "Tn7X" }
    , { libelle = "Douceur", id = Type.Id "5gDw" }
    , { libelle = "Empathie", id = Type.Id "UUK0" }
    , { libelle = "Fête", id = Type.Id "u9HO" }
    , { libelle = "Intimité", id = Type.Id "46YE" }
    , { libelle = "Joie", id = Type.Id "b1Sb" }
    , { libelle = "Partage", id = Type.Id "WXD-" }
    , { libelle = "Reconfort", id = Type.Id "dq_k" }
    , { libelle = "Tendresse", id = Type.Id "Zzqq" }
    ]


pouvoir : List Type.Item
pouvoir =
    [ { libelle = "Accomplissement", id = Type.Id "RC4L" }
    , { libelle = "Assurance", id = Type.Id "CEz7" }
    , { libelle = "Centrage", id = Type.Id "W3Pr" }
    , { libelle = "Détachement", id = Type.Id "A-yL" }
    , { libelle = "Estime De Soi", id = Type.Id "AtGv" }
    , { libelle = "Honnêteté", id = Type.Id "f7XR" }
    , { libelle = "Humilité", id = Type.Id "weIs" }
    , { libelle = "Intégrité", id = Type.Id "b94_" }
    , { libelle = "Sincerité", id = Type.Id "yDQI" }
    ]


reperes : List Type.Item
reperes =
    [ { libelle = "Apprentissage", id = Type.Id "vmnC" }
    , { libelle = "But", id = Type.Id "Je4x" }
    , { libelle = "Changement", id = Type.Id "jDsO" }
    , { libelle = "Clarification", id = Type.Id "CI2P" }
    , { libelle = "Compréhension", id = Type.Id "Xf98" }
    , { libelle = "Créativité", id = Type.Id "UtZj" }
    , { libelle = "Évolution", id = Type.Id "M899" }
    , { libelle = "Inspiration", id = Type.Id "b4Xb" }
    , { libelle = "Participation", id = Type.Id "Q9FM" }
    , { libelle = "Réalisation", id = Type.Id "XiOi" }
    , { libelle = "Structure", id = Type.Id "jLUg" }
    ]


securite : List Type.Item
securite =
    [ { libelle = "Abri", id = Type.Id "-9Xp" }
    , { libelle = "Bien être", id = Type.Id "3Imy" }
    , { libelle = "Confiance En Moi", id = Type.Id "sPLu" }
    , { libelle = "Défoulement", id = Type.Id "ZQW1" }
    , { libelle = "Détente", id = Type.Id "9pt8" }
    , { libelle = "Espace", id = Type.Id "Uppn" }
    , { libelle = "Lumière", id = Type.Id "jU_c" }
    , { libelle = "Mouvement", id = Type.Id "ocDo" }
    , { libelle = "Nourriture", id = Type.Id "KaFP" }
    , { libelle = "Protection", id = Type.Id "66_V" }
    , { libelle = "Relaxation", id = Type.Id "E1Fz" }
    , { libelle = "Repos", id = Type.Id "7-ss" }
    , { libelle = "Sécurité", id = Type.Id "giZp" }
    , { libelle = "Stabilité", id = Type.Id "-2tF" }
    , { libelle = "Tranquilité", id = Type.Id "nMmG" }
    , { libelle = "Vitalité", id = Type.Id "3qJG" }
    ]


sens : List Type.Item
sens =
    [ { libelle = "Conscience", id = Type.Id "o0la" }
    , { libelle = "Equilibre", id = Type.Id "TCqP" }
    , { libelle = "Harmonie", id = Type.Id "GJgl" }
    , { libelle = "Paix", id = Type.Id "ND1T" }
    , { libelle = "Ressourcement", id = Type.Id "CH8y" }
    , { libelle = "Silence", id = Type.Id "-mQH" }
    , { libelle = "Sens", id = Type.Id "qBqf" }
    , { libelle = "Spiritualité", id = Type.Id "eL2G" }
    , { libelle = "Transcendance", id = Type.Id "c2-c" }
    ]


realisation : List Type.Item
realisation =
    [ { libelle = "Acceptation", id = Type.Id "fbaN" }
    , { libelle = "Affirmation", id = Type.Id "Omtr" }
    , { libelle = "Autonomie", id = Type.Id "d5eT" }
    , { libelle = "Espace Pour Moi", id = Type.Id "5HiV" }
    , { libelle = "Indépendance", id = Type.Id "xmhZ" }
    , { libelle = "Lâcher Prise", id = Type.Id "DoC3" }
    , { libelle = "Liberté", id = Type.Id "pOTp" }
    , { libelle = "Spontanéité", id = Type.Id "8-uQ" }
    ]


communication : List Type.Item
communication =
    [ { libelle = "Collaboration", id = Type.Id "MGdM" }
    , { libelle = "Communication", id = Type.Id "MYip" }
    , { libelle = "Confidentialité", id = Type.Id "TyM1" }
    , { libelle = "Cooperation", id = Type.Id "04ij" }
    , { libelle = "Discrétion", id = Type.Id "2-hU" }
    , { libelle = "Échange", id = Type.Id "nkJd" }
    , { libelle = "Justice", id = Type.Id "PM-Y" }
    , { libelle = "Présence", id = Type.Id "i5pA" }
    , { libelle = "Reconnaissance", id = Type.Id "zpey" }
    , { libelle = "Respect", id = Type.Id "dxci" }
    ]


tous : Type.ItemsParCategorie
tous =
    [ ( Type.Categorie "Relation", relation )
    , ( Type.Categorie "Pouvoir", pouvoir )
    , ( Type.Categorie "Repères", reperes )
    , ( Type.Categorie "Sécurité", securite )
    , ( Type.Categorie "Sens", sens )
    , ( Type.Categorie "Réalisation", realisation )
    , ( Type.Categorie "Communication", communication )
    ]
        |> AssocList.fromList


libellesParId : AssocList.Dict Type.Id String
libellesParId =
    relation
        ++ pouvoir
        ++ reperes
        ++ securite
        ++ sens
        ++ realisation
        ++ communication
        |> List.map (\item -> ( item.id, item.libelle ))
        |> AssocList.fromList
