module Type exposing (..)

import AssocList


type Categorie
    = Categorie String


libelleCategorie : Categorie -> String
libelleCategorie (Categorie libelle) =
    libelle


type Id
    = Id String


idBrut : Id -> String
idBrut (Id brut) =
    brut


type alias Item =
    { id : Id, libelle : String }


type alias ItemsParCategorie =
    AssocList.Dict Categorie (List Item)
