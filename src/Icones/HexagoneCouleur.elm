module Icones.HexagoneCouleur exposing (..)

import Html exposing (Html)
import Svg exposing (path, svg)
import Svg.Attributes as SvgAttr


view : Html msg
view =
    svg
        [ SvgAttr.viewBox "4 0 134 136"
        , SvgAttr.fill "none"
        ]
        [ Svg.g
            [ SvgAttr.filter "url(#filter0_d_2_85)"
            ]
            [ path
                [ SvgAttr.d "M52.0576 6.6541C63.065 -1.45289 78.0834 -1.39248 89.0252 6.8028L123.428 32.5698C133.956 40.4555 138.361 54.127 134.418 66.676L121.967 106.294C117.908 119.212 105.934 128 92.3931 128H49.375C36.1334 128 24.3537 119.589 20.0544 107.065L6.39004 67.2592C1.99239 54.4484 6.42095 40.2657 17.3269 32.2334L52.0576 6.6541Z"
                , SvgAttr.fill "url(#paint0_linear_2_85)"
                ]
                []
            , path
                [ SvgAttr.d "M54.1332 9.47225C63.8978 2.28057 77.2206 2.33415 86.9271 9.60416L121.33 35.3712C130.669 42.3665 134.577 54.4945 131.079 65.6267L118.628 105.245C115.027 116.704 104.405 124.5 92.3931 124.5H49.375C37.6284 124.5 27.1787 117.039 23.3648 105.929L9.70043 66.1229C5.79928 54.7584 9.72784 42.177 19.4025 35.0516L54.1332 9.47225Z"
                , SvgAttr.stroke "black"
                , SvgAttr.strokeWidth "7"
                ]
                []
            ]
        , Svg.defs []
            [ Svg.filter
                [ SvgAttr.id "filter0_d_2_85"
                , SvgAttr.x "0.708435"
                , SvgAttr.y "0.614845"
                , SvgAttr.width "139.137"
                , SvgAttr.height "135.385"
                , SvgAttr.filterUnits "userSpaceOnUse"
                , SvgAttr.colorInterpolationFilters "sRGB"
                ]
                [ Svg.feFlood
                    [ SvgAttr.floodOpacity "0"
                    , SvgAttr.result "BackgroundImageFix"
                    ]
                    []
                , Svg.feColorMatrix
                    [ SvgAttr.in_ "SourceAlpha"
                    , SvgAttr.type_ "matrix"
                    , SvgAttr.values "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    , SvgAttr.result "hardAlpha"
                    ]
                    []
                , Svg.feOffset
                    [ SvgAttr.dy "4"
                    ]
                    []
                , Svg.feGaussianBlur
                    [ SvgAttr.stdDeviation "2"
                    ]
                    []
                , Svg.feComposite
                    [ SvgAttr.in2 "hardAlpha"
                    , SvgAttr.operator "out"
                    ]
                    []
                , Svg.feColorMatrix
                    [ SvgAttr.type_ "matrix"
                    , SvgAttr.values "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
                    ]
                    []
                , Svg.feBlend
                    [ SvgAttr.mode "normal"
                    , SvgAttr.in2 "BackgroundImageFix"
                    , SvgAttr.result "effect1_dropShadow_2_85"
                    ]
                    []
                , Svg.feBlend
                    [ SvgAttr.mode "normal"
                    , SvgAttr.in_ "SourceGraphic"
                    , SvgAttr.in2 "effect1_dropShadow_2_85"
                    , SvgAttr.result "shape"
                    ]
                    []
                ]
            , Svg.linearGradient
                [ SvgAttr.id "paint0_linear_2_85"
                , SvgAttr.x1 "36"
                , SvgAttr.y1 "24"
                , SvgAttr.x2 "115.5"
                , SvgAttr.y2 "116.5"
                , SvgAttr.gradientUnits "userSpaceOnUse"
                ]
                [ Svg.stop
                    [ SvgAttr.stopColor "#ACE1FF"
                    ]
                    []
                , Svg.stop
                    [ SvgAttr.offset "0.197917"
                    , SvgAttr.stopColor "#BDFFD7"
                    ]
                    []
                , Svg.stop
                    [ SvgAttr.offset "0.380208"
                    , SvgAttr.stopColor "#FAFFBD"
                    ]
                    []
                , Svg.stop
                    [ SvgAttr.offset "0.541667"
                    , SvgAttr.stopColor "#FFEAB5"
                    ]
                    []
                , Svg.stop
                    [ SvgAttr.offset "0.744792"
                    , SvgAttr.stopColor "#FFD7FB"
                    ]
                    []
                , Svg.stop
                    [ SvgAttr.offset "1"
                    , SvgAttr.stopColor "#D1A4FF"
                    ]
                    []
                ]
            ]
        ]
