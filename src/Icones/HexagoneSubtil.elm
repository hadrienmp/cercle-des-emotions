module Icones.HexagoneSubtil exposing (..)

import Html exposing (Html)
import Svg exposing (path, svg)
import Svg.Attributes as SvgAttr


view : Html msg
view =
    svg
        [ SvgAttr.viewBox "0 0 99 96"
        , SvgAttr.fill "none"
        ]
        [ Svg.g
            [ SvgAttr.filter "url(#filter0_d_2_169)"
            ]
            [ path
                [ SvgAttr.d "M60.7387 5.17767C53.8221 0.0956282 44.3959 0.133514 37.5204 5.27099L12.6677 23.8412C6.029 28.8018 3.25087 37.4166 5.74087 45.321L14.7307 73.8589C17.2883 81.9781 24.8171 87.5 33.3297 87.5L64.4396 87.5C72.7638 87.5 80.1702 82.2159 82.8786 74.3447L92.74 45.6853C95.5165 37.6163 92.7239 28.6792 85.8472 23.6265L60.7387 5.17767Z"
                , SvgAttr.stroke "black"
                , SvgAttr.strokeOpacity "0.03"
                , SvgAttr.shapeRendering "crispEdges"
                ]
                []
            ]
        , Svg.defs []
            [ Svg.filter
                [ SvgAttr.id "filter0_d_2_169"
                , SvgAttr.x "0.338623"
                , SvgAttr.y "0.891846"
                , SvgAttr.width "97.9638"
                , SvgAttr.height "95.1082"
                , SvgAttr.filterUnits "userSpaceOnUse"
                , SvgAttr.colorInterpolationFilters "sRGB"
                ]
                [ Svg.feFlood
                    [ SvgAttr.floodOpacity "0"
                    , SvgAttr.result "BackgroundImageFix"
                    ]
                    []
                , Svg.feColorMatrix
                    [ SvgAttr.in_ "SourceAlpha"
                    , SvgAttr.type_ "matrix"
                    , SvgAttr.values "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    , SvgAttr.result "hardAlpha"
                    ]
                    []
                , Svg.feOffset
                    [ SvgAttr.dy "4"
                    ]
                    []
                , Svg.feGaussianBlur
                    [ SvgAttr.stdDeviation "2"
                    ]
                    []
                , Svg.feComposite
                    [ SvgAttr.in2 "hardAlpha"
                    , SvgAttr.operator "out"
                    ]
                    []
                , Svg.feColorMatrix
                    [ SvgAttr.type_ "matrix"
                    , SvgAttr.values "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.33 0"
                    ]
                    []
                , Svg.feBlend
                    [ SvgAttr.mode "normal"
                    , SvgAttr.in2 "BackgroundImageFix"
                    , SvgAttr.result "effect1_dropShadow_2_169"
                    ]
                    []
                , Svg.feBlend
                    [ SvgAttr.mode "normal"
                    , SvgAttr.in_ "SourceGraphic"
                    , SvgAttr.in2 "effect1_dropShadow_2_169"
                    , SvgAttr.result "shape"
                    ]
                    []
                ]
            ]
        ]
