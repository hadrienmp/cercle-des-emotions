module Emotion exposing (libellesParId, toutes)

import AssocList
import Type


peur : List Type.Item
peur =
    [ { libelle = "Alarmé·e", id = Type.Id "bSpj" }
    , { libelle = "Angoissé·e", id = Type.Id "BQmC" }
    , { libelle = "apeuré·e", id = Type.Id "cg8t" }
    , { libelle = "choqué·e", id = Type.Id "mzqx" }
    , { libelle = "désorienté·e", id = Type.Id "yjxk" }
    , { libelle = "épouvanté·e", id = Type.Id "q2ig" }
    , { libelle = "inquiet·ète", id = Type.Id "mnpw" }
    , { libelle = "mal à l'aise", id = Type.Id "a9rt" }
    ]


tristesse : List Type.Item
tristesse =
    [ { libelle = "triste", id = Type.Id "1djz" }
    , { libelle = "anéanti·e", id = Type.Id "h3x_" }
    , { libelle = "déchiré·e", id = Type.Id "et5d" }
    , { libelle = "découragé·e", id = Type.Id "ruiv" }
    , { libelle = "déprimé·e", id = Type.Id "v91x" }
    , { libelle = "désespéré·e", id = Type.Id "yxic" }
    , { libelle = "malheureux·euse", id = Type.Id "aghq" }
    , { libelle = "mélancolique", id = Type.Id "4src" }
    , { libelle = "soucieux·euse", id = Type.Id "lj7m" }
    ]


colere : List Type.Item
colere =
    [ { libelle = "en colère", id = Type.Id "tsok" }
    , { libelle = "enragé·e", id = Type.Id "nn3p" }
    , { libelle = "fâché·e", id = Type.Id "96az" }
    , { libelle = "frustré·e", id = Type.Id "rnwz" }
    , { libelle = "furieux·euse", id = Type.Id "m_sr" }
    , { libelle = "irrité·e", id = Type.Id "1ic7" }
    , { libelle = "mauvaise humeur", id = Type.Id "i1ly" }
    , { libelle = "mécontent·e", id = Type.Id "jclf" }
    , { libelle = "nerveux·euse", id = Type.Id "expd" }
    ]


joie : List Type.Item
joie =
    [ { libelle = "heureux.euse", id = Type.Id "tbu8" }
    , { libelle = "bonne humeur", id = Type.Id "ua1z" }
    , { libelle = "confiant·e", id = Type.Id "hzts" }
    , { libelle = "content·e", id = Type.Id "us2n" }
    , { libelle = "enthousiaste", id = Type.Id "pmxa" }
    , { libelle = "euphorique", id = Type.Id "omip" }
    , { libelle = "joyeux·euse", id = Type.Id "vvqa" }
    , { libelle = "serein·e", id = Type.Id "b5je" }
    , { libelle = "vivant·e", id = Type.Id "8but" }
    ]


autres : List Type.Item
autres =
    [ { libelle = "coupable", id = Type.Id "qqaz" }
    , { libelle = "dégouté·e", id = Type.Id "m_6s" }
    , { libelle = "honteux·euse", id = Type.Id "tpgg" }
    , { libelle = "surpris·e", id = Type.Id "pzn0" }
    ]


toutes : Type.ItemsParCategorie
toutes =
    [ ( Type.Categorie "Joie", joie )
    , ( Type.Categorie "Peur", peur )
    , ( Type.Categorie "Colère", colere )
    , ( Type.Categorie "Tristesse", tristesse )
    , ( Type.Categorie "Autres", autres )
    ]
        |> AssocList.fromList


libellesParId : AssocList.Dict Type.Id String
libellesParId =
    joie
        ++ peur
        ++ colere
        ++ tristesse
        ++ autres
        |> List.map (\item -> ( item.id, item.libelle ))
        |> AssocList.fromList
