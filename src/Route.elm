module Route exposing (..)

import Url
import Url.Builder
import Url.Parser exposing ((</>))


type Route
    = RouteSelection
    | RouteResume (Maybe String)


parseRoute : Maybe String -> Url.Url -> Route
parseRoute maybeContext =
    Url.Parser.parse (routeParser maybeContext) >> Maybe.withDefault RouteSelection


routeParser : Maybe String -> Url.Parser.Parser (Route -> a) a
routeParser maybeContext =
    let
        top =
            case maybeContext of
                Just context ->
                    Url.Parser.s context

                Nothing ->
                    Url.Parser.top
    in
    Url.Parser.oneOf
        [ Url.Parser.map RouteSelection top
        , Url.Parser.map RouteResume (top </> Url.Parser.s "resume" </> Url.Parser.fragment identity)
        ]


toString : Maybe String -> Route -> String
toString context route =
    let
        ctx =
            context
                |> Maybe.map List.singleton
                |> Maybe.withDefault []
    in
    case route of
        RouteSelection ->
            Url.Builder.absolute (ctx ++ []) []

        RouteResume maybeHash ->
            Url.Builder.custom
                Url.Builder.Absolute
                (ctx ++ [ "resume" ])
                []
                maybeHash
