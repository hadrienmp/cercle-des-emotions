module Effect exposing (..)

import Route exposing (Route)


type Effect msg
    = Command (Cmd msg)
    | PushRoute Route
    | None


map : (a -> b) -> Effect a -> Effect b
map f effect =
    case effect of
        Command command ->
            Command <| Cmd.map f command

        PushRoute route ->
            PushRoute route

        None ->
            None
