module Extra.String exposing (..)


trimSlashes : String -> String
trimSlashes =
    dropFrontSlash >> dropEndSlash


dropFrontSlash : String -> String
dropFrontSlash context =
    case context |> String.toList of
        '/' :: value ->
            value |> String.fromList

        _ ->
            context


dropEndSlash : String -> String
dropEndSlash =
    String.reverse >> dropFrontSlash >> String.reverse
