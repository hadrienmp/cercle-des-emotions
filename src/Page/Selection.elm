module Page.Selection exposing (..)

import AssocList
import Besoin
import Browser
import Effect exposing (Effect)
import Emotion
import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Evts
import Page.Resume
import Route exposing (Route)
import Type exposing (Item)



-- Init


type alias Model =
    { emotions : ItemCochablesParCategorie
    , besoins : ItemCochablesParCategorie
    }


type alias ItemCochablesParCategorie =
    AssocList.Dict Type.Categorie (AssocList.Dict Type.Item Bool)


init : ( Model, Effect Msg )
init =
    ( { emotions = Emotion.toutes |> decocherTout
      , besoins = Besoin.tous |> decocherTout
      }
    , Effect.None
    )


decocherTout : Type.ItemsParCategorie -> ItemCochablesParCategorie
decocherTout =
    AssocList.map
        (always (List.map (\item -> ( item, False )) >> AssocList.fromList))



-- Update


type Msg
    = EmotionClickee Type.Categorie Type.Item
    | BesoinClicke Type.Categorie Type.Item


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        EmotionClickee categorie emotion ->
            ( { model
                | emotions =
                    AssocList.update categorie
                        (Maybe.map (AssocList.update emotion (Maybe.map not)))
                        model.emotions
              }
            , Cmd.none
            )

        BesoinClicke categorie besoin ->
            ( { model
                | besoins =
                    AssocList.update categorie
                        (Maybe.map (AssocList.update besoin (Maybe.map not)))
                        model.besoins
              }
            , Cmd.none
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : { context : Maybe String } -> Model -> Browser.Document Msg
view { context } model =
    { title = "Cercle des émotions"
    , body =
        [ Html.main_
            []
            [ vueEtape
                { items = model.emotions
                , onClick = EmotionClickee
                , titre = "1. Émotions"
                }
            , vueEtape
                { items = model.besoins
                , onClick = BesoinClicke
                , titre = "2. Besoins"
                }
            , Html.a
                [ Attr.href <| Route.toString context <| urlDuResume model
                , Attr.id "resume"
                ]
                [ Html.text "> Aller au résumé"
                ]
            ]
        ]
    }


urlDuResume : Model -> Route
urlDuResume model =
    Route.RouteResume
        (Just <|
            Page.Resume.toHash
                { emotions = itemsCoches model.emotions
                , besoins = itemsCoches model.besoins
                }
        )


itemsCoches : ItemCochablesParCategorie -> List Item
itemsCoches items =
    items
        |> AssocList.values
        |> List.concatMap (AssocList.filter (\_ checked -> checked) >> AssocList.keys)


vueEtape :
    { items : ItemCochablesParCategorie
    , onClick : Type.Categorie -> Type.Item -> Msg
    , titre : String
    }
    -> Html Msg
vueEtape { items, onClick, titre } =
    Html.section [ Attr.class "etape" ]
        [ Html.h1 [] [ Html.text titre ]
        , Html.div [ Attr.class "categories" ]
            (items
                |> AssocList.map (vueCategorie onClick)
                |> AssocList.values
            )
        ]


vueCategorie : (Type.Categorie -> Type.Item -> Msg) -> Type.Categorie -> AssocList.Dict Type.Item Bool -> Html Msg
vueCategorie onClick categorie items =
    Html.section []
        [ Html.h2 [] [ Html.text <| Type.libelleCategorie categorie ]
        , Html.div []
            (items
                |> AssocList.map (vueItem (onClick categorie))
                |> AssocList.values
            )
        ]


vueItem : (Type.Item -> Msg) -> Type.Item -> Bool -> Html.Html Msg
vueItem onClick item checked =
    let
        (Type.Id id) =
            item.id
    in
    Html.label
        [ Attr.for id
        , Attr.checked checked
        , Evts.onInput <| always <| onClick item
        ]
        [ Html.input [ Attr.type_ "checkbox", Attr.id id ] []
        , Html.text item.libelle
        ]
