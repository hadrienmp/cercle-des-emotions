module Page.Resume exposing (..)

import AssocList
import Besoin
import Browser
import Effect exposing (Effect)
import Emotion
import Html
import Html.Attributes
import Icones.HexagoneCouleur
import Icones.HexagoneSubtil
import Route
import Shared exposing (Shared)
import Type exposing (Id)



-- Init


type alias Model =
    { emotions : List String, besoins : List String }


init : Maybe String -> ( Model, Effect Msg )
init maybeHash =
    case maybeHash |> Maybe.andThen parseHash of
        Just ids ->
            ( { emotions =
                    ids.emotions
                        |> List.filterMap (\id -> AssocList.get id Emotion.libellesParId)
              , besoins =
                    ids.besoins
                        |> List.filterMap (\id -> AssocList.get id Besoin.libellesParId)
              }
            , Effect.None
            )

        Nothing ->
            ( { emotions = [], besoins = [] }
            , Effect.PushRoute Route.RouteSelection
            )



--


type alias ParsedHash =
    { emotions : List Id, besoins : List Id }


parseHash : String -> Maybe ParsedHash
parseHash hash =
    case String.split "&" hash of
        [ emotionsBrut, besoinsBrut ] ->
            Just
                { emotions =
                    emotionsBrut
                        |> String.replace "emotions=" ""
                        |> String.split ","
                        |> List.map Type.Id
                , besoins =
                    besoinsBrut
                        |> String.replace "besoins=" ""
                        |> String.split ","
                        |> List.map Type.Id
                }

        _ ->
            Nothing


toHash : { emotions : List Type.Item, besoins : List Type.Item } -> String
toHash { emotions, besoins } =
    "emotions="
        ++ (emotions
                |> List.map (.id >> Type.idBrut)
                |> String.join ","
           )
        ++ "&besoins="
        ++ (besoins
                |> List.map (.id >> Type.idBrut)
                |> String.join ","
           )



-- Update


type Msg
    = Noop


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )



-- View


view : Shared -> Model -> Browser.Document Msg
view { context } model =
    { title = "Résumé"
    , body =
        [ Html.header []
            [ Html.a [ Html.Attributes.href <| Route.toString context Route.RouteSelection ]
                [ Html.div [ Html.Attributes.id "logo" ] [ Icones.HexagoneCouleur.view ]
                , Html.h1 []
                    [ Html.span [] [ Html.text "Prisme" ]
                    , Html.span [] [ Html.text " Empathie" ]
                    ]
                ]
            ]
        , Html.main_ [ Html.Attributes.id "page-resume" ]
            [ Html.div [ Html.Attributes.id "resume" ]
                [ viewSection
                    { icon = Html.text "❤️"
                    , title = "Je me sens"
                    , values = model.emotions
                    }
                , viewSection
                    { icon = Html.text "👐"
                    , title = "J'ai besoin de"
                    , values = model.besoins
                    }
                ]
            ]
        ]
    }


viewSection : { icon : Html.Html msg, title : String, values : List String } -> Html.Html msg
viewSection { icon, title, values } =
    Html.section []
        [ Html.div [ Html.Attributes.class "icone" ]
            [ Html.div
                [ Html.Attributes.class "arriere" ]
                [ Icones.HexagoneSubtil.view
                ]
            , Html.div
                [ Html.Attributes.class "avant" ]
                [ icon ]
            ]
        , Html.div []
            [ Html.h1 [] [ Html.text title ]
            , Html.ul []
                (values
                    |> List.map
                        (Html.text
                            >> List.singleton
                            >> Html.li []
                        )
                )
            ]
        ]
