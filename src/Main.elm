module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Css exposing (selection)
import Effect exposing (Effect)
import Extra.String
import Html
import Page.Resume
import Page.Selection
import Route
import Shared exposing (Shared)
import Url
import Url.Parser exposing ((</>))



-- MAIN


type alias Flags =
    { context : Maybe String }


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias Model =
    { url : Url.Url
    , key : Nav.Key
    , shared : Shared
    , page : Page
    }


type Page
    = Selection Page.Selection.Model
    | Resume Page.Resume.Model


initPage : Shared -> Url.Url -> ( Page, Effect PageMsg )
initPage { context } url =
    case Route.parseRoute context url of
        Route.RouteSelection ->
            Page.Selection.init
                |> Tuple.mapBoth
                    Selection
                    (Effect.map SelectionMsg)

        Route.RouteResume maybeHash ->
            Page.Resume.init maybeHash
                |> Tuple.mapBoth
                    Resume
                    (Effect.map ResumeMsg)


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        shared =
            { context = flags.context |> Maybe.map Extra.String.trimSlashes }

        ( page, pageCmd ) =
            initPage shared url
    in
    ( { url = url
      , key = key
      , shared = shared
      , page = page
      }
    , pageCmd
        |> Effect.map PageMsg
        |> perform shared key
    )


perform : Shared -> Nav.Key -> Effect Msg -> Cmd Msg
perform { context } key effect =
    case effect of
        Effect.None ->
            Cmd.none

        Effect.Command command ->
            command

        Effect.PushRoute route ->
            Route.toString context route |> Nav.pushUrl key



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | PageMsg PageMsg


type PageMsg
    = SelectionMsg Page.Selection.Msg
    | ResumeMsg Page.Resume.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                ( page, pageCmd ) =
                    initPage model.shared url
            in
            ( { model | url = url, page = page }
            , pageCmd
                |> Effect.map PageMsg
                |> perform model.shared model.key
            )

        PageMsg pageMsg ->
            updatePage pageMsg model.page
                |> Tuple.mapBoth (\next -> { model | page = next })
                    (Cmd.map PageMsg)


updatePage : PageMsg -> Page -> ( Page, Cmd PageMsg )
updatePage pageMsg page =
    case ( pageMsg, page ) of
        ( SelectionMsg subMsg, Selection selection ) ->
            Page.Selection.update subMsg selection
                |> Tuple.mapBoth Selection (Cmd.map SelectionMsg)

        ( ResumeMsg subMsg, Resume resume ) ->
            Page.Resume.update subMsg resume
                |> Tuple.mapBoth Resume (Cmd.map ResumeMsg)

        _ ->
            ( page, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Browser.Document Msg
view model =
    mapView PageMsg <|
        case model.page of
            Selection selection ->
                Page.Selection.view model.shared selection
                    |> mapView SelectionMsg

            Resume resume ->
                Page.Resume.view model.shared resume
                    |> mapView ResumeMsg


mapView : (a -> b) -> Browser.Document a -> Browser.Document b
mapView f { title, body } =
    { title = title
    , body = body |> List.map (Html.map f)
    }
