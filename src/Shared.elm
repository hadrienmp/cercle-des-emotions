module Shared exposing (..)


type alias Shared =
    { context : Maybe String }
